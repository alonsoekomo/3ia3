# **codewars**

## **katas**

## Enunciado
escribir una función maskifyque cambie todos los caracteres excepto los últimos cuatro a '#'. de una tarjeta de crédito.

---


##  Solución


def maskify(cc):
    l = len(cc)
    if l <= 4: return cc
    return (l - 4) * '#' + cc[-4:]

```
 

```

## Enunciado
Se le dará una matriz de números enteros. Su trabajo es tomar esa matriz y encontrar un índice N donde la suma de los enteros a la izquierda de N sea igual a la suma de los enteros a la derecha de N. Si no hay un índice que haga que esto suceda, devuelva: -1.


---

---
## Solución 

def find_even_index(arr):
    for i, _ in enumerate(arr):
        if sum(arr[:i]) == sum(arr[i+1:]):
            return i
    return -1

---


## Enunciado

Escriba una función que tome una lista de cadenas y devuelva cada línea precedida por el número correcto.

La numeración comienza en 1. El formato es n: string. Observe los dos puntos y el espacio en el medio.

## Solucion

def number(lines):
       return ["{}: {}".format(str(i+1), c) 
       for i,c in enumerate(lines)]

---
---

## Enunciado 

Se le da una matriz de números enteros de longitud impar , en la que todos ellos son iguales, excepto por un solo número.

Complete el método que acepta dicha matriz y devuelve ese único número diferente.

---
## Solucion

def stray(arr):
    for i in arr:
        if arr.count(i) == 1:
            return i



## Enunciado 
Su tarea es escribir una función que devuelva la suma de las siguientes series hasta el término n (parámetro).
Si el valor dado es 0, entonces debería devolver 0.00
Series: 1 + 1/4 + 1/7 + 1/10 + 1/13 + 1/16 +...


## Solución

def series_sum(n):
    listSeries = [1, 1/4, 1/7, 1/10, 1/13, 1/16]
    sum = 0.00
    for i in range(n):
        sum += listSeries[i]
    return str("{:.2f}".format(sum))













