#!/usr/bin/env python3

#print es imprimir 
print('hola mundo')
print()                   #espacios en blanco

#variable es un contenedor contiene valores y se usa 
mensaje = 'hola camilo'
print(mensaje)

nemero = 4

mensaje = "hola joseba"
print(mensaje)
x,y,z = 1,2,3
print(x)
print(y)
print(z)

#modo compacto de asignar el mismi valor a 3 variables

x = y = z = 12

#Ojo el prit mete \n al final por defecto
print(x)
print(y)
print(z)


# NUEMEROS 
numero = 12
print(type(numero)) #<class 'int'>  int significa integer (entero)


print(isinstance(numero,int))  #true 

print(isinstance('',int))       #false

# operaciones aritmeticas
print(4+3) 
print(4-3)
print(2*3)
print(2**3)    #al cubo
print(3/2)
print(5%2)     #es el resto 
print(5//2)


print((2*3)-1) # 

# Float = coma flotante =  numeros  con decimales

w = 12.4
print(type(w)) #<class 'float'> 


#Advertencia
resultado = 12 / 6
print(type(resultado))  #<class 'float'>   coma flotante 


#STRING = STR = cadenas de texto

"hola Iker"
'hola Iker'
print("hola tu nombre es \"Iker\"?")
print("hola tu nombre es:\n\ \"Iker\"?")

cadena1 = "hola"
cadena2 = " camilo"

cadena_resultante = cadena1 + cadena2 
print(cadena_resultante)

print("cadena"*3)  #es multiplica cadena por 3


# CAMBIO MAYUSCULA, MINUSCULA
name = "ada lovelace"
print(name.title())          # la primera letra en maysucula
print(name.capitalize())     # la primera letra en 
print(name.upper())          #  todo en mayusculas 
print("ADA".lower())         # todo en minusculas





# Ejercicio 

nombre = "camilo"
saludo = " hola amigo " + nombre
print(saludo)

# Calcular la longitud de una cadena 

len("cinco")
print(len("cinco")) # 5


cadena = " uno "
print(len(cadena.strip()))  # 3 strip:elimina espacios por delante y detras

print(len(cadena.lstrip())) # elimina espacios en blanco por la izquierda

print(len(cadena.rstrip())) # elimina espacios en blanco por derecha 




# BUSCAR 
cadena =  'spam'
print(cadena.find('a'))


# Remplazar cadena
print(cadena.replace('pa','XYZ'))


line = 'aaaaa;bbbb;cccc'  # divide la cadena en 3 subcadenas
print(line.split(';'))












#Indexar

cadena = "TRES"
print(cadena[0])
print(cadena[1])
print(cadena[2])
print(cadena[3])
print(cadena[-1])




# inmutabilidad

# SLICING

cadena = "cinco"
print(cadena[0])

# hacen lo mismo
print(cadena)
print(cadena[:])
print(cadena[0:5]) 
print(cadena[-1])


print(cadena[1:4])     


# BOOLEANOS

True
False
booleano = True
print(type(booleano))


var1=True
var2=False
# 00
# 01
# 10
# 11

# AND 
print(False and False) #false
print 
print


# OR 
print(False or False)  #False
print(False or True)   #True













