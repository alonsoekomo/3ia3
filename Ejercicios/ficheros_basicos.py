#!/usr/bin/env python3

# Ejercicio 1

def sintaxAnalyzer(archivo):    
        lineas = archivo.readlines()
        print (lineas)




# Ejercicio 2 

from itertools import islice

def file_read_from_head(fname, nlines):
    with open(fname) as f:
        for line in islice(f, nlines):
            print(line)


with open('test.txt', 'w') as f:
    f.write("Primera linea \n"
            "segunda linea \n"
            "tercera linea \n")


file_read_from_head('test.txt', 2)














































