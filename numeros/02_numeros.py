#!/usr/bin/env python3

# Operaciones aritméticas
print(1+2)
print(2*3)
print(2**3) #8
# https//wwww.w3schols.com/python/gloss_python_arithmetic_operators.asp

print(5/2)

print(7//2) # Floor division

import math 
print(math.pi)
print(math.sqrt(4)) # 2.0

import random
print(random.random())

# Borrar una variable
vartest =22323
del(vartest)
# ERROR! ya no existe 
#print(vartest)

# CASTING
num=123
print(type(str(num)))


