#!/usr/bin/env python3

# Mi primera variable
mensaje = "Hola mundo!"

print(mensaje)

print = 'cruel'

print(mensaje)

# python es tipado dinamico, puede cambiar su tipo de valor
# en tiempo de ejecucion
myVar = 21
myVar = "string"

#obtener el tipo de variable 

print(type(myVar))

# <clash 'str'>
# str es la simplificación de string

# Asignación múltiple
# Mismo valor para diferentes variables

x = y = z = 10
print(x)
print(y)
print(z)
# En una misma línea valores y variables diferentes

a,b,c ='camilo',13.11,12
print(c)
print(y)


print(isinstance(3.14,float)) #true(boolean)
print(isinstance(y,init)) #true
print(isinstance(y,float)) 

