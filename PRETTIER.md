# **PRETTIER** 

## **Resume** 

Es una extencion de code visual que permite formatear el código según las reglas de cada lenguaje. Soporta la sintaxis de una gran cantidad de lenguajes, como JavaScript

## **Enlace**
[URL] (https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)

## **Instalación** 

Se instala a través de extensiones de VS Code. BuscarPrettier - Code formatter
también se puede instalar en VS Code: inicie VS Code Quick Open (Ctrl+P), pegue el siguiente comando y presione Intro.


## **Características**

Puede usar la configuración de VS Code para configurar más bonito. Los ajustes se leerán desde (enumerados por prioridad) y tambien propociona métodos abreviados de teclado para dar formato al código. Puede obtener información sobre estos para cada plataforma en la documentación de VS Code .

## **Uso**

Uso de la paleta de comandos (CMD/CTRL + Shift + P)

ejemplo:
CMD + Shift + P -> Format Selection

## **Conclusion**

En conclusion no parece que sea muy útil a primera vista
